from django.forms import ModelForm
from .models import Kegiatan, Peserta

class KegiatanForm(ModelForm):
    class Meta:
        model = Kegiatan
        fields = '__all__'
        
class PesertaForm(ModelForm):
    class Meta:
        model = Peserta
        fields = '__all__'