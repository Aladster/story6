from django.test import LiveServerTestCase, TestCase, tag , Client
from django.urls import reverse
from selenium import webdriver
from django.urls import resolve
from .views import createKegiatan, hasil , tambahPeserta
from .models import Kegiatan, Peserta
from .forms import KegiatanForm, PesertaForm



@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:kegiatan'))
        self.assertEqual(response.status_code, 200)


class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())

# Create your tests here.
class Teststory6(TestCase):
    def test_url_(self):
        response = Client().get('//')
        self.assertEqual(response.status_code, 200)
        
    def test_url_create_kegiatan(self):
        response = Client().get('/create_kegiatan/')
        self.assertEqual(response.status_code, 200)
        
    def test_url_output(self):
        response = Client().get('/output/')
        self.assertEqual(response.status_code, 200)
        
 #   def test_cek_Tambah_peserta(self):
  #      response = Client().get('/tambah_peserta/')
 #       html = response.content.decode('utf8')
  #      self.assertIn("Tambah peserta", html)
        
#    def test_cek_add_kegiatan(self):
#        response = Client().get('//')
 #       html = response.content.decode('utf8')
 #       self.assertIn("Nama kegiatan", html)
        
    def test_template_kegiatanku(self):
        response = Client().get('//')
        self.assertTemplateUsed(response, 'main/kegiatan.html')
    


        
        
        