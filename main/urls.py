from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.kegiatan, name='kegiatan'),
    path('create_kegiatan/',views.createKegiatan, name='create_kegiatan'),
    path('tambah_peserta/',views.tambahPeserta, name='tambah_peserta'),
    path('output/',views.hasil, name='hasil'),
]
