from django.shortcuts import render,redirect 
from django.http import HttpResponse
from .forms import KegiatanForm, PesertaForm
from .models import Kegiatan , Peserta


def kegiatan(request):
    return render(request, 'main/kegiatan.html')

def createKegiatan(request):
    form = KegiatanForm()
    isi = {'form' : form}
    if request.method == "POST":
        form = KegiatanForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')
            
    return render(request, 'main/kegiatan_form.html',isi)


def tambahPeserta(request):
    form = PesertaForm()
    isi = {'form' : form}
    if request.method == "POST":
        form = PesertaForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')
            
    return render(request, 'main/peserta_form.html',isi)

def hasil(request):
    kegiatan = Kegiatan.objects.all()
    peserta = Peserta.objects.all() 
    isi = {
            'Kegiatan' : kegiatan,
           'Peserta' : peserta,
           }
    if request.method == "POST":
        form = tambahPeserta(request.POST)
        if form.is_valid():
            return redirect('/')
            
    return render(request, 'main/output.html',isi)